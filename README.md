# blog.estoff.cc generator

blog.estoff.cc generator is a shell script which generates a simple static blog
using [Pandoc](https://pandoc.org).

## Getting Started

Please fork the repo and use this as a base template for a blog generator.
This shell script is inspired by similar projects for static site generation
using pandoc such as [tundra.sh](https://frainfreeze.github.io/tundra/),
[pdsite](http://pdsite.org/), and [Pandoc Bash
Blog](https://github.com/bewuethr/pandoc-bash-blog).

### Prerequisites

To run blog.estoff.cc generator, the following software must be installed.
Follow the links to the homepage of each program for installation instructions
for your platform.

* [awk](https://www.gnu.org/software/gawk/manual/gawk.html)
* [Pandoc](https://pandoc.org/)

### Installing

Use blog.estoff.cc generator as a base template by cloning the Git repository.

1. Clone this repository, and make edits as desired.

```bash
$ git clone https://gitlab.com/estoff.cc/blog.estoff.cc
```

2. Build a new blog.

```bash
$ ./build # From the project root directory
```

*Optional*: Automate the Linting and Deployment by editing `gitlab-ci.yml`.

## Linting

It is recommended that you
[lint](https://en.wikipedia.org/wiki/Lint_(software))
the build script before execution after any edits have been made. Run lint with
[shellcheck](https://www.shellcheck.net/).

```bash
$ shellcheck build
```

## Deployment

To deploy, create the copy all files to a directory, and host that directory
with a web server.

```bash
$ mkdir public
$ cp -r * public # From the project root directory
```

## Versioning

This project uses [Semantic Versioning](http://semver.org/). Versions are
tracked with [tags](https://gitlab.com/estoff.cc/blog.estoff.cc/-/tags).

## Authors

* **Denzel Ayala** - *Frontend* - <https://gitlab.com/denzelayala1>
* **Weston Odend'hal** - *Backend* - <https://gitlab.com/wodend>

## License

This project is licensed under the MIT License, see [LICENSE.txt](LICENSE.txt)
for details.

## Acknowledgments

* [Dave Jarvis](https://dave.autonoma.ca/) for the bash template used for the
  build script
* [tundra.sh](https://frainfreeze.github.io/tundra/)
* [pdsite](http://pdsite.org/)
* [Pandoc Bash Blog](https://github.com/bewuethr/pandoc-bash-blog).
