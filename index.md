---
title: blog.estoff.cc
authors:
    - Denzel Ayala <denzelayala1@gmail.com>
    - Weston Odend'hal <wodend@tuta.io>
date: 2020-04-22
---

# About

This page is a work in progress, for now there is only one sample post.

# Posts
* [Gnome Foundation Proposal](post/20200701-gnome-foundation-proposal.html)
