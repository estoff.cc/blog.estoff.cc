---
title: Gnome Challenge Proposal
authors:
    - Denzel Ayala <denzelayala1@gmail.com>
    - Weston Odend'hal <wodend@tuta.io>
date: 2020-07-01
---

# 1 Our Idea
## 1.1 What is the working name of your project?
estoff.cc

## 1.2 Tell us in 200 words or less about your idea. (Feel free to include key features, unique elements, how it will meet challenge goals, etc.)
estoff.cc aims to ease the deployment of research software for scientific
computing using Free and Open Source Software (FOSS). It will provide images
for deploying [Quantum Espresso](https://www.quantum-espresso.org/) using
[Docker Compose](https://docs.docker.com/compose/). This project is intended to
align with several of the goals outlined in the [Quantum Espresso development
roadmap](https://www.quantum-espresso.org/resources/road-map), such as
**Improve build mechanism**, **Improve packaging**, and **Tools for
verification**. Listed in the table below is the software used in our stack
with links to source and license information.

| Title | Source | License |
| ----- | ------ | ------- |
| Certbot | <https://github.com/certbot/certbot> | [Apache License 2.0](https://github.com/certbot/certbot/blob/master/LICENSE.txt) |
| Docker Compose | <https://github.com/docker/compose> | [Apache License 2.0](https://github.com/docker/compose/blob/master/LICENSE)
| GitLab | <https://gitlab.com/gitlab-org/gitlab> | [GitLab](https://gitlab.com/gitlab-org/gitlab/-/blob/master/LICENSE) |
| GitLab Runner | <https://gitlab.com/gitlab-org/gitlab-runner> | [MIT](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/LICENSE) |
| HAProxy | <https://git.haproxy.org/> | [HAProxy](https://git.haproxy.org/?p=haproxy.git;a=blob;f=LICENSE;h=717e303580be3f01d45793517ee1c3b5ea937f42;hb=HEAD) |

# 2 Project Details
## 2.1 How does your entry introduce the FOSS community to beginning coders?
This project indends to introduce a FOSS software stack to scientific
programmers, many of whom are beginning coders.

## 2.2 As part of this challenge we seek ideas that engage diverse populations that are often under-represented in the coding community. Will your submission engage members of this audience?
estoff.cc hopes to make deploying software easier for those with limited
resources. Enabling the use of a cloud platform allows developers to scale
their infrastructure as they gain resources to do so. We are doing this with a
focus on easily deploying the software locally as well to self-host the
software.

## 2.3 Introducing coders to the FOSS community is important, but engaging and retaining them within the community is also crucial. Please explain how your project will engage and retain users.
Denzel will start contributing with GitLab and learn Git.

## 2.4 How will your project’s impact will be measured?
Site traffic should give us a rough idea of the exposure of some of the ideas
we would like to spread about FOSS in the scientific community.

## 2.5 What did we miss? Is there anything you want to add about your submission?
This is a part of our larger ambition to introduce the scientific community to
FOSS and DevOps.

## 2.6 Supplemental materials are encouraged but not required. Please upload at most 5 files
Some ideas:

* blog-post-weston
* blog-post-denzel
* Denzel's proposal
* docker-compose.yml
* haproxy.cfg

# 3 Our Team
## 3.1 About Us
*  Weston Odend'hal  
   wodend@tuta.io  
   415-672-7969  
   United States  
   FOSS contributor.

*  Denzel Ayala  
   denzel.ayala1@gmail.com  
   United States  
   818-855-4835
   FOSS community member.

## 3.2 Are you working with a team?
The team consists of Denzel and Weston. We can be found on GitLab:
[estoff.cc developers](https://gitlab.com/estoff.cc-developers).

## 3.3 We would like to know more about you/your team’s background and experiences. Feel free to share relevant skills, examples of past projects, hobbies and other interests.
**Weston**
I am interested in security, DevOps, software development. It was a joy to set up a site using some of the latest best practices from industry.

**Denzel**
I am currently pursing a Ph.D in Materials Science. I spent the early years of my research career in a lab making 
small molecules as an organic chemist in training but as I learned more about what was avaiable in the scientific world I have begun to gravitate towards the intersection of theory and experimentation. 

In this pursuit, I have been learning how to do scientific programing. It has been a joy learning about the history of computer science and by extension the FOSS comunity.


## 3.4 Documentation and record keeping are key parts of staying organized. Explain how you’ll document your work so that others may contribute to future iterations.
We plan on providing detailed tutorials and blog posts about all of the
development activities on estoff.cc.

## 3.5 How did you learn about the challenge?
The Linux for Everyone podcast.

## 3.6 Within your team, how will the roles and responsibilities between the members of your team be allocated? (please skip if you are working individually)
*  Denzel - Chemistry, physics, mathematics, programming
*  Weston - Networking, security, DevOps, programming
